# ADAM MUSEUM DESIGN

Une collaboration a été faite entre les étudiants en architectures et le musée du design. Le but est de trouver un alternatif pour adapter le musée à la visite des enfants. L'idée est créée une visite qui ne dérangeraient pas les visiteurs, les membres du personnel et qui soit ludique et adapter à autant aux enfants qu'au musée.

![](./images/projetfinal/meee.jpg)


![](./images/projetfinal/bouer.jpg)

Nous avons appris que le musée à pu être mis en place grâce à la collection que Phillipe Decelle qui a récupéré des pièces de collection rare dans les poubelles de la ville. Au fil du temps cette passion a pris de l'ampleur à fin d'en faire une collection exceptionnelle.

![](./images/projetfinal/objett.jpg)

![](./images/projetfinal/objet1.jpg)

## Recherche et visite du musée.

Avant de commencer ce projet, on a eu pendants plusieurs semaines des formations pour nous les modules I et nous avons rejoint le projet des modules II une fois que nous avons été formées. Chaque étudiante de modules II nous a présenté son idée. Louise avec ces formes réduites des objets du musée, Maxime avec son idée de productions des objets du musée avec leur technique de production, Eliott avec sa visite 3D et enfin Tehjay qui crée une sorte de chasse à l'objet à travers le musée.

J'ai directement apprécié son idée que je vous détaille maintenant. L'idée est de créer des cartes qui représente le lieu et l'époque de certain objet dans le musée. Afin de créer un négatif afin que les enfants puissent chercher l'objet à travers les cartes dans le musée et le retrouver dans son contexte. Le tout inspiré du monde de Pokémon moi qui ai été une grande Fan de cette animation. De plus Tehjay partais avec une problématique celle du fait que son idée était un jeu et que pour le musée il n'était pas concevable que les enfants cours partout, touches ou font trop de bruit. Ce genre de choses est très compliqué, je pensais pouvoir être utile, ayant eu une formation en animation brevetée par l'ONE.
![](./images/projetfinal/silhouette.png)


## Introduction à notre jeu, notre monde, le monde du design.

Pour que les enfants soient concentré et rentre dans ce monde aussi complique que celui du Design, le plastique et musée. Il faut leur expliquer une histoire avec des mots simples. Celle-ci va même me permettre d'introduire notre thématique sur mon Git et d'expliquer le scénario.

### Histoire
#### Introduction au musée

Cher ami , bienvenue dans ce nouveau monde celui du design.
Toutes les choses que vous allez découvrir aujourd’hui on commence grâce à un aventurier.
On va vous raconter son histoire son nom est **Philippe Decelle**, sa passion dans la vie est le plastique.

 On pose les questions pour animer la conversation :
Qui sait me dire ce que c’est du plastique ?
Maintenant qui se me dire où l'on peut retrouver le plastique ?

Bon retournons à l’histoire de notre aventurier, un jour en se promenant dans la rue il a découvert une chose extraordinaire, la chaise de **Joe colombo** .

Comme des acteurs, on montre la chaise de manière fantastique à travers nos cartes.

Il se donna comme mission de récupérer tous les objets célèbre pour en faire une collection unique. Et décida de partir à la découverte d’objet rare  dans ces promenades pleines d’aventures.

Au fur et à mesure il trouve ces trésors cacher dans les poubelles de la ville.

Ici nous avons assemblé tous ces objets rares et hors du commun, car notre PROFESSEUR COLLECTIONNEUR nous a chargé de trouver **les futurs collectionneurs** de demain.
Aujourd’hui les amies notre objectif est de vous transformer en débutant collectionneur comme notre aventurier et savoir si vous êtes prêts à reprendre le relai.   (donner un rôle à l'enfants dans l'histoire, un objectifs a atteindre)

#### Comment on va faire ça ?

Chacun d’entre vous va recevoir une boite remplie de cartes qui s’appelle le **collecteur**.
Il existe deux types de boites. On va donc crée deux équipes.
Les deux équipes devront trouver les objets caché. Les cartes sont donc différentes par équipe.
Ensuite on va vous laisser vous promener et essayer de retrouver les objets rares de notre collectionneur en regardant à travers les **cartes** dans le musée. Une petite astuces , sur les cartes sont dessiner l’environnement naturelle de l’objet. Une fois l’objet observé, vous devrait trouverez un **coffre** ou sont cacher les badges.

#### Comment savoir si c’est le bon coffre ?
Placer votre cartes sur le coffre et voyez si celui ce complète avec le dessin. Ensuite pour offrir le coffre  il suffit de répondre aux **égnimes**, afin de déverrouille  le coffre  et récupérer votre badges. Et prenez en un pour compléter votre collecteur.  Attention de bien refermer le coffre pour que les badges ne se retrouvent pas dans des mauvaise mains. N’oublier pas de remettre la carte dans le collecteur et partez à la recherche du prochain objet.

#### Comment gagner la Partie?

 il faut nous ramener le collecteur rempli 10 badges manquants, chacun de ces badges représente un type de plastique de la collection de notre Professeur designer.

L’acteur mimes les règles la découverte et montre comment on trouve et mets le badge dans le collecteur.

#### Mais attention, attention, attention …

Nous les CHAMPIONS DESIGNERS
Pouvons vous stopper dans le jeu  
Si vous ne respecter pas 3 règles

La première est de pas courir
La deuxième est de pas toucher
Et la 3e est de pas crier, mais de chuchoter.

Si c’est 3 règles ne sont pas respecté vous les Débutant collectionneur, vous serez immobilisé pendant 60 sec .

Est-ce que c’est compris ? Est-ce qu’il y a des questions ?

#### Pour finir la partie,
il faut remplir le collecteur avec ces bons badges, ramener au PROFESSEUR MASTER DESIGN qui vous donnera votre récompense pour commencer votre propre collection
rendez-vous dans la plasticothèque ou ailleurs(selon le musée).

### Lien entre Pokémon et la mission design museum
### Conception:
Pour que le jeu fonctionne on a donc dû créé et imaginer les différents éléments expliquer dans notre histoire en sous point ci-dessous. C'est-à-dire :


##### L'histoire
L'introduction de chaque jeu Pokémon commence par l'explication de l'aventure du top dresseur ou du professeur, ici nous nous commençons avec l'histoire de  Philippe Decelle qui est présenté comme le professeur Chen, c'est eux qui nous introduisent dans l'histoire qui donne la mission à un jeune enfants pour reprendre la relève.
![](./images/projetfinal/professeurs.jpg)
Phillipe Decelle - Professeur Chen

##### La collection
On parle bien sûr de la collection des objets trouver de Phillipe Decelle. Mais dans Pokémon, le professeur Chen capture les Pokémon pour apprendre tout sur eux. Et crée une forme de collection qu'il appelle le Pokédex. Celui-ci est classé par type de Pokémon, comme les Pokémon feu, eau, électrique ou terre. On sait dis qu'on allait diviser la collection de Philippe par type de plastiques. Chacun des plastiques sera représenter par une forme géométrique.
![](./images/projetfinal/collection.png)


### Le collecteur
Le collecteur est la sacoche en plastique que va récupérer les visiteur à l'entrée. Elle va contenir les cartes dans le musée, celui  est percer pour laisser place ou logo qui peuvent être remplis part des badges (expliquer dans la partie badge).

A l'avant nous avons décider de crées une ouverture pour avoir facile d'accès pour les cartes car le jours de notre crash teste nous avons vue que les enfants avaient une difficulté à la récupérer.  On a décider pour la version finale de crée une coque en PLA qui a été imprimé dans les machines 3D. et dans les quel se clips des facettes en Plexiglas sur la quelles ont on graver le logo du musée et les espaces pour les badges. Celle ci afin de simplifier le travaille après le jeu. Cela permets de simplement retirer les facettes à la fin deux jeu.
J'ai travaillé sur cette coque et fais plusieurs évolutions pour l'impression 3D:
##### Le modèle 3D  
<iframe src="https://icloud46911.autodesk360.com/shares/public/SH9285eQTcf875d3c539fa214cf4dd257208?mode=embed" width="640" height="480" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>
 Ce modèle la devait s'accrocher au pantalon comme les premier prototype totalement en plexi. Le probleme c'est que c'est ni pratique ni confortable. L'utulisateur avait besoin de l'avoir facilement en maus sans le décroché de son pantalon.


<iframe src="https://icloud46911.autodesk360.com/shares/public/SH9285eQTcf875d3c539b2d7a89acdaa1945?mode=embed" width="640" height="480" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>
Trop de matière inutile et support ne permettant pas une impression correcte. L'alignement des boucle et le modele fini est pas au TOP.

<iframe src="https://icloud46911.autodesk360.com/shares/public/SH9285eQTcf875d3c539048e856367811ebe?mode=embed" width="1024" height="768" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>
Version complete et finale, produit fini, il y'a une continuité, simplicité qui le rend léger visuellement
![](./images/projetfinal/facecollecteur.png)

Dans la dernière version, nous avons ajouté des boucles sur les côtés à la place des crochets qui devait s'attacher au pantalon. Ces boucles permettent d'y ajouter des bandoulières ( voif git [Arno](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-design/students/arno.reggiani/))  .

![](./images/projetfinal/cotecollecteur.png)

![](./images/projetfinal/arrcollecteur.png)

### Les cartes
Le but premier des cartes est de trouver les objets à travers le musée. Donc nous avons affectueux un premier teste pour voir si cela est possible.

![](./images/projetfinal/testecarte.png)

 Comment dessiner nos cartes, on a d'abord fait une rapide analyse de celle produite par Pokémon.


Les cartes sont représentées comme les cartes Pokémon avec des informations similaire exemple :
les cartes Pokémon donne leurs noms et une photo du Pokémon dans son milieu. En haut un gauche un logo montre le type du logo et le niveau Pokémon.
![](./images/projetfinal/cartepoke.jpg)
90cm X 65cm`

Nos cartes donnent les noms des créateurs en haut à droite et celui de l'objet en haut à gauche. La forme de l'objet est représenté dans leur milieu. De plus en bas à gauche de nos cartes, on peut retrouver la forme du badge qui correspond à la famille de plastique auquel il appartient. En bas à droite l'année de sa création.
![](./images/projetfinal/carteavt.jpg)
![](./images/projetfinal/cartearr.jpg)

Nous nous sommes repartie le travail en groupes chacun avait sept cartes à travailler pour ma part, c'était celle-ci dessous.

![](./images/projetfinal/carte4.png)
![](./images/projetfinal/carte5.png)

Pour la **Ove**, étant une lumière qui avait pour but de s'inspirer de la lune j'ai trouvé sa chouette de mettre des références à Star Trek, afin que les adultes ont la petite référence pop culture.

Pour la **chaise Charlie** j'ai mis sur le papier à dessin de RALPH un dessin animé Disney le premier à regrouper tous les dessins animé Disney et à expliquer aux enfants comment fonction l'internet et le virtuel. J'ai ajouté la table de la collection de la chaise, car je trouve ça vraiment impressionnant le fait qu'on voit que le plastique, c'est recyclé avec ces différentes particules.

Pour la **Molar** et la **Saissi** c'est chaise datant des années 70 j'ai essayé de reproduire le papier peint en vogue à ce moment-là avec ces géométries assez atypiques. De même pour la **Alba** ou là par contre j'ai laissé un mur de pierre brique avec une affiche des Beatles pour la sortie de leur album sortie cette année-là.


### Les badges
Chacun des badges représente un type de plastique. Celle-ci ont été entièrement penser par [Tehjay](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-design/students/tejhay.pinheiroalbia/final-project/).
 Il a aussi confectionné différents types de coffres.
Le coffre est quant à lui inspirer de notre imaginaire, l'idée est que les égnimes poser dessus permettent aux enfants d'apprendre quelque chose sur l'objet.
### From Youtube

<iframe width="560" height="315" src="https://www.youtube.com/watch?v=qZI-NeNpWso&ab_channel=tjalbia" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

### Présentation jury

![](./images/projetfinal/tfa.png)


## Lien des Gits de mes collaborateurs

- [Tehjay](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-design/students/tejhay.pinheiroalbia/final-project/)
- [Arno](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-design/students/arno.reggiani/)
