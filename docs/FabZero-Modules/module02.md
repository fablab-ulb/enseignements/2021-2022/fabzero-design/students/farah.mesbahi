# Module II: Conception Assistée par Ordinateur

Pour ce nouveau module, nous avons eux droit à une formation sur Fusion par un professionnel, Thibaut Baes.

## Installation de Fusion

Rien de plus simple il suffit de se rendre sur le site Autodesk et télécharger fusion via ce
[lien](https://www.autodesk.com/products/fusion-360/personal).


Une fois le fichier télécharger je l'ai ouvert avec un double clics pour que celui-ci s'installe sur mon MacBook.

Une fois installer il m'a suffi de l'ouvrir ou là, on me demande de rejoindre ou crée une équipe. → j'ai créé une équipe.

## Modéliser sur fusion

##### Créer une esquisse :
On commence toujours par un dessin 2D. Il faut donc se poser sur un des plans cartésien sur lequel on veut travaille.  J'ai une préférence pour le X et Y. Pour cela il suffit de cliquer sur l’une des faces du carré qui se situe au centre.
![](../images/module2/esquisse.png)
##### Outils nécessaires pour le dessin 2D :
Une fois sur notre plan, on a accès à plusieurs outils. Tels que :

- **Créer** qui nous permets d'utiliser des fonctions de dessin afin de "créer". On retrouve comme choix : des lignes, des rectangles, des courbes, des cercles, etc.
-**Modifier** : comme son nom l'indique, là on retourne des fonctions paramétriques qui permet de modifier le dessin. Et là, les choix sont coupé, prolonger, décaler, etc. tous les éléments déjà dessinés.
-**Contrainte** : Les contraintes permettent de mettre des règles (= des contraintes) pour s'assurer que le dessin garde certaines propriétés nécessaires à la modélisation. Comme garder des droites des parallèles, perpendiculaire ou garder un côté à une certaine mesure, etc.
-**Inspecter** : Cet fonction nous permets de vérifier les dimensions et les proportions de nos réalisations.

A la fin il suffit de cliquer sur ``Terminer l'ésquisse``.

![](../images/module2/dessin2d.png)

##### Outils nécessaires pour le dessin 3D :
- **Extruder** : Avec cette fonction, on a un double effet :

La première épaissir :

Pour ça il faut partir d'une surface plane et simplement cliquer dessus et augmenter l'épaisseur.

La seconde est de d'épaissir et/ou donc creuser dans une forme :
Pour ça il suffit de partir d'une surface 2D sur l'objet et de d'épaissir, c'est-à-dire épaissie d'un chiffre négatif, pour creuser.

À chaque fois qu'on utilise cette fonction, on doit complète certaines propriétés que fusion nous demande qui sont :  
Le point de départ, la direction de l’extrusion (à sens unique, à double sens ou symétrique), le point de fin, la distance de l'extrusion, l’angle et enfin l’opération (joindre, couper, croiser, nouveau body, nouvelle composante)

 - **révolution** : C'est un autre outil pour créer un objet en 3D. Celui ci permet de faire tourner un profil 2D autour d’un axe choisi.

- **Réseau** : Réseau permet de dupliquer , elle est hyper intéressante sur 3D, car elle permet donc du multiplié les extrusions, faces, corps, … selon un réseau rectangulaire, circulaire ou une trajectoire souhaitée.

 - **balayage** :  Cela nous permet donc de balayer un profil 2D le long d'une trajectoire choisi. Il suffit de sélectionner le profil que l'on veut faire bouger et choisir la ligne (à dessiner) qui va conduire sur le long d'un chemin.
 
![](../images/module2/dessin3d.png)

## Modélisation en 3D de la H-horse de Nendo.

 ![](../images/module2/module21.jpg)
 Donc un retour sur la chaise à bascule pour enfants du designeur du bureau Nendo. Sur leur site, j'ai trouvé toutes les informations sur les dimensions et j'ai vu une image parallèle à la figue.

 J'ouvre Fusion et je clique sur `insère` pour importer l'[image](../images/module2/module21.jpg).
![](../images/module2/module22.png)


Ensuite fusion me permets de faire quelque réglage pour géré la dimension et sur quels repères cartésiens mettre mon image pour commencer à travailler dessus.
![](../images/module2/module23.png)

Une fois l'image insérée et à l'échelle, je clique sur conception et sur la ligne pour décalquer sur mon dessin.
![](../images/module2/module23a.png)


![](../images/module2/module23b.png)

Ensuite j'ai été sur `maillage`, ensuite j'ai cliqué sur `modifier` et j'ai choisi l'option de `mise à l'échelle`.
![](../images/module2/module23c.png)

Au départ j'ai extrudé (épaissir) la forme complète mais je n'arrivais pas avoir ma ligne au milieu.
![](../images/module2/module23d.png)

Et donc j'ai remcommencé par extrudé  juste le haut et le bas de 40 cm
![](../images/module2/module23e.png)

Ensuite j'ai crée un autes corps (en jaunes) pour le mettre au milieu de la base de mon objet et le support assise.
![](../images/module2/module25.png)


## Le modèle 3D   
<iframe src="https://icloud46911.autodesk360.com/shares/public/SH9285eQTcf875d3c539e6f2b9b4d186193e?mode=embed" width="1024" height="768" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>

J'ai suivi ce [tuto](https://www.youtube.com/watch?v=lWDh7XZASoA) proposer sur le [Gitlab](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-design/students/amina.belguendouz/FabZero-Modules/module02/#le-modele-3d) d'Amina Belguendouz.

Le [fichier stl](../fichier/hhorsenendo.stl) à télécharger.
