# Module III: Impression 3D

Cette semaine, on a commencé le cours en téléchargeant Prusar qui est un programme qui nous permets d'imprimer l'objet dessiner en 3D :

## Explication du Fablab

Petite conférence avec Gwen qui nous explique comment fonctionne le Fablab.
On a droit à deux objets gratuit par semaine et en cas de demande de plastique spéciale, on peut toujours aller dans leur bureau.

## Passer de fusion à l'impression

Une fois notre objet dessiner sur fusion, on doit l'exporter en STL afin que celui-ci puisse être installé sur le programme qui permets l'impression.

 [Fichier STL](../fichier/hhorsenendo.stl)


![](../images/objetfusionstl.png)

## Installation du Programme Prusar et configuration de la machine

##### Étape I : Installe Prusar via son site web.

 Aller sur leur [ site web](https://www.prusa3d.com) pour télecharger Prusar.

Une fois installer et ouvert on a choisi d'installer les modèles les réglages de toutes les imprimantes.

On a utilisé les imprimantes du Fablab donc de l'original Prusar MK3.

##### Étape II: Réglage pour l'impression.
J'ai importé mon objet stl sur Prusar simplement en le glissant de dans.

![](../images/objetprusar1.png)

Pour commencer les réglages il faut d'abord choisir l'imprimante, celle du fablab est :

 `` 0.20mm QUALITY @MK3 (modifié)``

 Une fois le plateau choisi on clique sur réglage d'impression :

On clique sur couche et périmètre et la faut modifier aucune information sauf dans la couche solide, j'ai mis 3 mm et 8 mm afin de solidifier mon objet.
![](../images/reglage2.png)



Ensuite dans remplissage pour avoir une impression rapide, j'ai choisi rectiligne, ils sont classés du plus rapide au moins rapide et du moins solide au plus solide.
![](../images/reglage3.png)

 J'ai généré des supports automatique aussi vue la forme complexe et les bords en porte-à-faux. Qui vont se générer en vert tant dis que mon objet sera en orange.
![](../images/reglage5.png)

Je l'ai exporté en G-code pour obtenir mon temps d'impression qui est de 56 min.

Le fichier [G-code](../fichier/hhorse.gcode)

##### Étape III:
 Une fois le fichier G-code enregistré sur le MacBook on le met sur la carte mémoire de la machine qui va directement nous demander de choisir un fichier dans mon cas le dossier est ``Farah ``.

![](../images/machine1.jpg)

Avant de lancer le fichier, il faut nettoyer la plaque avec de l'acétone.

![](../images/machine2.jpg)

Une fois le fichier lancer l'imprimante se mets à chauffer une fois arrivé à 60° elle se met à imprimer automatiquement.

![](../images/machine4.jpg)
La vitesse de départ est de 100 %, mais une fois l'impression bien lancer on peut l'augmenter par proportion de 50 %. Personnellement vue la légèreté de mon objet, j'ai augmenté que de 20 % soit une impression de 120 % qui m'a pris un temps finale de 43 minutes.

##### Vidéo

![](../images/film.mp4)

## Résultat final

![](../images/resultat3d.jpg)
