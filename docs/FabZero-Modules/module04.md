# Module IV:  Découpe assistée par ordinateur

Cette semaine, on a eu comme objectif d'apprendre à utilisé les machines qui permettrais les impressions laser et sur vinyle.

## Comment fonctionne une machine d'impression à découpes laser

La machine est composée d'une pointe lumineuse qui est aussi source de chaleur qui va bruler en suivant un plan vectoriel. Plus la source bouge lentement et est plus puissante plus elle va creuser dans la matière et en faire une découpe. Tandis que si on inverse les réglages, c'est-a-dire qu'on la rend l'impression plus rapide, mais moins puissante celle-ci va pas découper, mais graver dans la matière.


## Comment réaliser une impression à découpes laser

##### Dessin vectoriel
Il faut savoir que les impressions fonctionnent en suivant un plan cartésien qui est donc composé de cordonnées qui va d'un point x à un point y. Pour pouvoir imprimer il faut donc avoir un dessin de type vectoriel, donc composer de ligne droite qui sont tracé suit à des cordonnées cartésiennes.
![](../images/module4/vectoriel.jpg)

Pour réaliser celui-ci il faut utiliser différents programmes qui permettent de dessiner. Pour ma part j'utilise le programme AutoCAD car j'y suis à l'aise avec . Celui-ci me permet d'exporter mes dessins dans le format que je préfère. Celui qui marche le plus avec les machines d'impression laser est le SVG ou DFX.

## Les machines du Fablab de l'ULB:

### Lasersaur
![](../images/module4/gprimante.png)
 Surface de découpe : 122 x 61 cm

- Hauteur maximum : 12 cm
- Vitesse maximum : 6000 mm/min
- Puissance du LASER : 100 W
- Type de LASER : Tube CO2 (infrarouge)

### EpilogFusion
![](../images/module4/pimprimante.png)


Importer le fichier dans la machine au moment des réglages ont choisi une couleur pour chaque intensité ou vitesse qu'on pourra la ligne.

- Surface de découpe : 81 x 50 cm
- Hauteur maximum : 31 cm
- Puissance du LASER : 60 W
- Type de LASER : Tube CO2 (infrarouge)

Il faut savoir que le Fablab crée des manuels en ligne qui explique comment fonctionne chacune de leurs machines. Et je conseille toujours de les relire avant l'utilisation d'une entre-elle.

### Tutoriel d'utilisation

[Tutoriel lasersaur]( https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Lasersaur.md)

[Tutoriel Epilogfusion](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/EpilogFusion.md)

## Test d'impression :

L'exercice était de tester différente puissance ainsi que sa rapidité du laser pour voir le résultat sur la matière utilisé.

On nous a fournis ce [fichier](https://fablab-ulb.gitlab.io/enseignements/2021-2022/fabzero-design/students/amina.belguendouz/images/module-04/calibration_grid.svg) qui quand on l'ouvre il ressemble à ça : ![](../images/module4/fichier.png)

 Chaque couleur à son réglage
 le premier carré en haut par exemple où on voit que la puissance est de 4 et rapidité de 10 ce qui va réaliser une gravure vue que la rapidité est plus forte que la puissance.

![](../images/module4/teste.jpg)

## La lampe :
### Dessin 2D :

J'ai dessiné ma lampe sur AutoCAD et exporter le [fichier](../images/module4/lamp.dxf) en dxf.

Pour éviter le gaspillage, j'ai divisé la surface de ma feuille en deux et ajouter les cercles à découper liée par des lignes au bord pour faire glisser mon plastique autour de mon système lumineux.

J'ai réservé la lasersaur qui elle ne prend que les fichiers svg. J'ai donc exporté le [fichier](../images/module4/lamp.svg) en svg.

![](../images/module4/lamp.png)

### Impression :
J'ai mis ma clef usb sur l'ordinateur du Fablab et j'ai ouvert mon fichier sur driveboardApp server.

J'ai choisi mes intensités. 1200 en puissance et 35 en rapidité pour éviter les traces de brulures.
![](../images/module4/aetape1.jpg)

On a suivis le [tutoriel lasersaur]( https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Lasersaur.md) fourni par le FABlab.

On a d'abord regardé si le laser était bien positionner et s'il fessait bien le contour.

![](../images/module4/testeimpression.mov)

L'impression a durée 3 minutes et 4 secondes

### Assemblage :
![](../images/module4/etape1.jpg)
On retourne le coter BC car la matière est différentes à l'arrière ce qui va permettre d'avoir un effet un peu plus opaque au croisement.

![](../images/module4/rotation.mov)
On rentre d'abord le côté A et puis le B.

![](../images/module4/etape2.jpg)

Ensuite comme sur la photo, on fait rentrer le coter D, mais par l'arrière.et qui passe endessou du A et B
![](../images/module4/etapes3.jpg)

Pour finir, comme sur la photo, on fait rentrer le coter C, mais par l'arrière D.
![](../images/module4/etape3a.JPG)

![](../images/module4/etape4.JPG)

Sur mon système lumineux il y avait deux viseurs de support vendu avec je l'ai est juste serré pour support même si ça tenait déjà.


![](../images/module4/fin.JPG)

### Résultat final

![](../images/module4/finale.mov)

![](../images/module4/final.jpg)
