#  Module I: Documentation

Le premier exercice avait pour but de se familiariser avec la documentation open-source, le GitLab et contrôle de version. Aussi de comprendre la nécessite du gitlab qui sera l’explication de notre processus durant notre formation en design et qui permettra de partager nos différents savoirs avec notre groupe et toutes personnes extérieur qui s’intéresse.
## Installation et configuration de GIT

##### ÉTAPE I: Installation du Git

D’abord, étant sur MacBook, il a fallu d’abord ouvrir le terminal et entrer la commande :

``git --version. ``.

A ce moment-là on m’a directement proposé d’installer le programmes Git.

##### ETAPE II: Configuration du Git

 Une fois installer il a fallu connecter le site au programme installer à mon ordinateur avec ces deux commandes :

 ``git config --global user.name "Farah.Mesbahi``

`` git config --global user.email "farahmesbahi98@gmail.com" .``

 Une fois cela fait-on clôture par :

``git config --global --list ``

À ce moment-là, mon terminale me montre, c’est résultats qui montrent une connexion à mon compte

##### ETAPE III: La cléfs SSH
Une fois connecter il faut aussi lier mon ordinateur a mon Gitlab afin de procéder aux modifications directement via mon MacBook pour cela j’avais besoin d’une clé SSH que j’ai obtenue grâce à la commande

``ssh-keygen -t ed25519 -C "<comment>"``

 Après ça le terminal génère une clef et la copies directement

 ![](../images/module3keys.png)

Je l'ai collé sur la demande clefs SSH sur mon profil et là. Ensuite j'ai cliquer sur cloner et mes fichiers GitLab sont apparue dans mon bureau.

##### ETAPE IV: Installation d'Atom

En allant simplement sur le site, j’ai téléchargé Atom sur le [lien](https://atom.io)

Ensuite j’ai synchronisé ouvert les fichiers Gitlab et commencer par éditer mon GitLab

## Personnaliser son site

Pour pourvoir mettre sont git avec une mise en page similaire au mien il faut dans le fihcier Le fichier [mkdocs.yml] et mettre les même information que moi que j'ai entouré.

 ![](../images/mkdock.png)

## Atom et le MarkDown

##### ETAPE I: Importer le fichier pour bosser sur son Git.

Une fois Atom installer, il suffit de l'ouvrir et sur macbook aller sur le coin gauche en haut 'file' et cliquer que 'add project folder'.
![](../images/importatom.png)


 Choisir son fichier et cliquer sur ouvrir.
![](../images/fichierchoix.png)


Le fichier s'ouvre sur Atom il suffit de cliquer sur les modules pour pouvoir écrire .
![](../images/ecrire.png)

##### ETAPE II: Markdown
Une fois qu'on peut enfin commencer a développer nos processus et explique notre formation design.  On doit respecte des codes propres a Atom qui vont nous permettre de structurer le site, comme des titres, écrire en gras, importer des liens ou des images. C'est codes sont repris sur le Markdown que je vous affiche ici.

![](../images/markdown.png)

## FETCH, PULL et PUSH

Une fois le texte est écrit il faut savoir les synchronisé et pour ca nous avons besoin de c'est 3 mots clefs FETCH, PULL et PUSH.

FETCH : Permet d'être sur que nous somme bien sur la dernière version.

PULL: Permet de récupérer la dernière version en ligne.

PUSH: Permet d'envoyer les modifications faites en local c'est-a-dire sur Atom.


Pour les macbook il faut aller sur file et save all . On constate que les fichiers modifier s'affichent à droite en jaunes avec un cercle remplis et les images ajoutés en vert avec un + .

On clique sur `stage all`
![](../images/atomenrengistre.png)

Tout se déplace automatiquement et il faut noter un petit commentaire j'ai mis module 1 et `commit to main`
![](../images/committo.png)

Il suffit de clique sur push pour que les textes se synchronise sur le site. `Push1`: push pour que les textes se synchronise sur le site.
![](../images/push.png)
