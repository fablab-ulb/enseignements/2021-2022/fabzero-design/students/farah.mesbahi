
## QUI SUIS-JE ?

![](./images/moi.jpg)

Bonjour, je suis Farah Mesbahi et je suis étudiante en master 1 en Architecture à la Faculté La cambre de l'ULB. Pendant mes humanités, j'ai eux une introduction à l'architecture et au sens de l'objet en suivant des études qualifiante de dessinateur en construction et travaux publics. Ceux-ci m'ont initié au dessin assisté par ordinateur et j'ai donc une très bonnes notions des programmes comme Autocad, sketchup et Photoshop. En plus de ça j'ai fait une formation en Animation agrée par l'ONE afin d'exercer le métier d'animatrice durant les vacances scolaire ce qui mon permis de travailler en m'amusant.

## MON HISTOIRE

L’architecture a pris un sens dans ma vie à l’âge de 10 ans. En effet, suite à des recherches dans les journaux pour un déménagement, j’ai compris que le métier d’architecte ne produisait pas qu’un simple toit ou une boite où y vivre, mais il offre plutôt une expérience de vie. Je voulais être une architecte, c’est la raison pour laquelle a l’âge de 14 ans, je me suis inscrite dans une option en sciences industrielles et en construction. Je fus diplômée en tant que dessinatrice en construction et travaux publics. Durant cette période-là, un nouveau passe-temps, c’est ouvert à moi. Ce dernier est celui du partage culturel et des voyages. J’ai eu l’occasion de partir au Canada, aux États-Unis et au Maroc. Lors de ces voyages, j’ai rencontré des jeunes de mon âge et partagé avec eux de forts moments d’échange. J’ai compris que la plus belle des richesses était l’échange du savoir. Par conséquent, j’ai décidé que je voulais être à la fin de mes études une architecte travaillant à l’international afin d’avoir l’opportunité de participer à des projets se trouvant partout dans le monde.
Étant de nature très curieuse, j’ai ce rêve de parcourir le monde, d’enrichir mes expériences et mes connaissances pour comprendre la société dans laquelle je vis, et ainsi mieux répondre aux futures attentes du métier d’architecte. Ces trois années m’ont appris que la patience et la capacité à s’adapter sont des qualités indispensables pour réussir en architecture. Chaque jour est pour moi un nouveau défi à relever grâce à ces études hors du commun.

 ![](./images/archi.png)
 
*Exemple de conceptionde projet fait par moi en BA1 à l'UCL*
## Enfants & Brussels Design Museum



L’idée est d’accommoder une partie du musée pour les enfants. Afin que ceux-ci se sentent aussi concerné par le design et qui comprennent le concept de celui-ci. Les musées sont peu accommodés pour les enfants et ceux-ci ne sont pas libre de toucher, essayer et s’amuser. Ce qui est étrange vue que le but du musée est d’instruire et que l’instruction est quelque chose ou les enfants devraient être les premières cibles. Notre but sera donc de concevoir cet espace qui sera dédier à l’amusement, au savoir et surtout au pratique. Un lieu ou l’art du design sera pratique et amusant.

![](./images/hhorse.jpg)

Une visite au musée nous a permis de choisir un premier objet. J’ai choisi le cheval à bascule faite par le bureau Nendo qui était en collaboration avec Kartell. Cet objet sera nommé H-horse et ce H provient directement de l’inspiration de la création qui ressemble à la poutre en H que l’on retrouve dans les structures architecturales.
 Pourquoi ce choix ? Ce fut un coup de foudre, cet objet relie ma passion pour l’architecture, les enfants et les chevaux. Je me suis directement identifié à celui-ci.
![](./images/nendoschemas.jpg)
